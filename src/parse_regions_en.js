module.exports = async (browser) => {
  console.group('Parsing english version...')

  const url = 'https://en.wikipedia.org/wiki/United_Nations_geoscheme'

  const page = await browser.newPage()
  await page.goto(url, { waitUntil: 'domcontentloaded' })

  const data = await page.evaluate(() => {
    return Array
      .from(document.querySelectorAll('h2 > span.mw-headline > a'))
      .map(item => {
        const name = item.textContent.trim()
        const href = item.href
        return {
          name,
          href,
        }
      })
  })

  await page.close()

  console.log('Done.')
  console.groupEnd()

  return data
}
