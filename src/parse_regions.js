const writeToFile = require('./utils/write_to_file')
const parseEnVersion = require('./parse_regions_en')
const parseRuVersion = require('./parse_regions_ru')

module.exports = async (browser) => {
  console.group('Parsing regions of the world...')

  let data
  data = await parseEnVersion(browser)
  data = await parseRuVersion(browser, data)

  await writeToFile('regions.json', JSON.stringify(data))

  console.log('Done.')
  console.groupEnd()
}
