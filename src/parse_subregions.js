const writeToFile = require('./utils/write_to_file')
const parseEnVersion = require('./parse_subregions_en')
const parseRuVersion = require('./parse_subregions_ru')

module.exports = async (browser) => {
  console.group('Parsing subregions of the world...')

  let data
  data = await parseEnVersion(browser)
  data = await parseRuVersion(browser, data)

  await writeToFile('subregions.json', JSON.stringify(data))

  console.log('Done.')
  console.groupEnd()
}
